class AddDefaultValueToVotes < ActiveRecord::Migration
  def up
    change_column :ideas, :votes, :integer, :default => 0
  end
  
  def down
    change_column :ideas, :votes, :integer, :default => nil
  end
end
