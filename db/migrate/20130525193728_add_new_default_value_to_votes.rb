class AddNewDefaultValueToVotes < ActiveRecord::Migration
  def change
    change_column :ideas, :votes, :integer, :default => 0
  end
end
