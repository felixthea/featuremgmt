class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :text
      t.integer :revenue_opp, :null => false, :default => 0

      t.timestamps
    end
  end
end
