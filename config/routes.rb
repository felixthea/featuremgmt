Featuremgmt::Application.routes.draw do
  root :to => "ideas#index"
  resources :ideas do
    resources :comments
    member do
      get 'upvote'
      post 'select_tag'
      post 'deselect_tag'
    end
  end
  resources :tags
  resources :idea_tags
end
