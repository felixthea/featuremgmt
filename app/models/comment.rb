class Comment < ActiveRecord::Base
  attr_accessible :revenue_opp, :text, :idea_id
  belongs_to :idea
end
