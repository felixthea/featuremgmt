class Idea < ActiveRecord::Base
  attr_accessible :description, :name, :votes
  has_many :comments, :dependent => :destroy
  has_many :idea_tags
  has_many :tags, :through => :idea_tags
end
