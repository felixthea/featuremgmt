class Tag < ActiveRecord::Base
  attr_accessible :name
  has_many :idea_tags
  has_many :ideas, :through => :idea_tags
end
