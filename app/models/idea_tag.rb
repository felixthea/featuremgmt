class IdeaTag < ActiveRecord::Base
  # attr_accessible :idea_id, :tag_id
  belongs_to :idea
  belongs_to :tag
end
