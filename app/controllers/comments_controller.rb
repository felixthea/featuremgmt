class CommentsController < ApplicationController
  def index
    @idea = Idea.find(params[:idea_id])
    @comments = Comment.find_all_by_idea_id(@idea.id)
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ideas }
    end
  end
  
  def new
    @idea = Idea.find(params[:idea_id])
    @comment = Comment.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @idea }
    end
  end
  
  def show
    @idea = Idea.find(params[:idea_id])
    @comment = Comment.find(params[:id])
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @comment }
    end
  end
  
  def create
    idea = Idea.find(params[:idea_id])
    @comment = Comment.new(params[:comment])
    @comment.idea = idea #associates the comment with the right idea
    
    respond_to do |format|
      if @comment.save
        format.html { redirect_to idea, notice: 'Comment was successfully created.' }
        format.json { render json: @comment, status: created, location: @idea }
      else
        format.html { render action: "new" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @comment = Comment.find(params[:id])
    
    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
        
end
