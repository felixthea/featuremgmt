class IdeasController < ApplicationController
  def index
    @ideas = Idea.all
    
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ideas }
    end
  end
  
  def show
    @idea = Idea.find(params[:id])
    @comments = Comment.find_all_by_idea_id(@idea.id) 
    @comment = Comment.new
    @tags = Tag.all
    @idea_tags = @idea.tags
    #@idea_tags = Idea_Tag.find_all_by_idea_id(@idea.id)
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @idea }
    end
  end
  
  def new
    @idea = Idea.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @idea }
    end
  end
  
  def create
    @idea = Idea.new(params[:idea])
    
    respond_to do |format|
      if @idea.save
        format.html { redirect_to @idea, notice: 'Idea was successfully created.' }
        format.json { render json: @idea, status: created, location: @idea }
      else
        format.html { render action: "new" }
        format.json { render json: @idea.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def upvote
    @idea = Idea.find(params[:id])
    @ideas = Idea.all
    
    if @idea.votes != nil
      @idea.votes += 1
    else
      @idea.votes = 1
    end
    @idea.save
    
    redirect_to ideas_path
  end
  
  def select_tag
    tags = Tag.find(params[:tag_id]) #this is an array of all the tags they selected in the multi-select
    idea = Idea.find(params[:idea_id])
    
    tags.each do |tag|
      tag_exists = false
      ideatags = IdeaTag.find_all_by_idea_id(idea.id)
      
      ideatags.each do |ideatag|
        if ideatag.tag_id == tag.id
          tag_exists = true
        end
      end
      
      if tag_exists == false
        idea.tags << tag
      end
    end
    redirect_to idea
  end
  
  def destroy
    @idea = Idea.find(params[:id])
    @idea.destroy
    
    respond_to do |format|
      format.html { redirect_to ideas_path }
      format.json { head :no_content }
    end
  end
  
  # def deselect_tag
  #   #@idea = Idea.find(params[:id])
  #   idea = Idea.find(params[:i_id])
  #   tag = Tag.find(params[:t_id])
  #   
  #   idea.tags.find(tag.id).destroy
  #   
  #   redirect_to idea
  # end
    
end