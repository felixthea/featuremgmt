class IdeaTagsController < ApplicationController
  def index
    @ideatags = IdeaTag.all
  end
  
  def create
    @ideatag = IdeaTag.new(:tag_id => :t_id, :idea_id => :i_id)
    @ideatag.save
  end
  
  def destroy
    idea = Idea.find(params[:i_id])
    tag = Tag.find(params[:t_id])
    
    idea_tag = IdeaTag.find_by_idea_id_and_tag_id(idea.id, tag.id)
    idea_tag.destroy
    
    redirect_to idea
  end
end